#[macro_use]
extern crate clap;
#[macro_use]
extern crate itertools;

use chinstrap as csp;

pub mod golomb;
pub mod magic_hexagon;
pub mod n_queens;
pub mod queen_armies;

pub trait Example {
    fn name(&self) -> &'static str;
    fn subcommand(&self) -> clap::App<'static, 'static>;
    fn handle_matches(&self, matches: &clap::ArgMatches) -> Result<(), csp::PropError>;
}

fn main() {
    let (golomb, golomb_subcommand) = self::golomb::subcommand();
    let magic_hexagon = self::magic_hexagon::MagicHexagon;
    let (n_queens, n_queens_subcommand) = self::n_queens::subcommand();
    let (queen_armies, queen_armies_subcommand) = self::queen_armies::subcommand();

    let matches = clap::App::new(crate_name!())
        .author(crate_authors!("\n"))
        .version(crate_version!())
        .subcommand(golomb_subcommand)
        .subcommand(magic_hexagon.subcommand())
        .subcommand(n_queens_subcommand)
        .subcommand(queen_armies_subcommand)
        .setting(clap::AppSettings::DisableHelpSubcommand)
        .setting(clap::AppSettings::SubcommandRequiredElseHelp)
        .get_matches();

    if let Some(matches) = matches.subcommand_matches(golomb) {
        self::golomb::handle_matches(matches);
    } else if let Some(matches) = matches.subcommand_matches(magic_hexagon.name()) {
        magic_hexagon.handle_matches(matches).unwrap();
    } else if let Some(matches) = matches.subcommand_matches(n_queens) {
        self::n_queens::handle_matches(matches);
    } else if let Some(matches) = matches.subcommand_matches(queen_armies) {
        self::queen_armies::handle_matches(matches);
    }
}
