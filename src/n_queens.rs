use chinstrap as csp;
use itertools::Itertools;

pub fn subcommand() -> (&'static str, clap::App<'static, 'static>) {
    let subcommand = clap::SubCommand::with_name("n_queens")
        .about("Solves the n queens puzzle")
        .arg(
            clap::Arg::with_name("count")
                .short("c")
                .long("count")
                .help("Counts the number of solutions"),
        ).arg(
            clap::Arg::with_name("N")
                .help("The size of the board")
                .default_value("8"),
        );
    ("n_queens", subcommand)
}

pub fn handle_matches(matches: &clap::ArgMatches) {
    let n = matches
        .value_of("N")
        .unwrap()
        .parse()
        .expect("could not parse N as an integer");
    assert!(n > 0, "N must be positive");

    let (model, vars) = model(n).expect("bad model");
    if matches.is_present("count") {
        println!("{}", model.solve().count());
    } else {
        print_soln(&model.solve().next().unwrap(), n, &vars);
    }
}

pub fn model(n: i64) -> Result<(csp::Model<csp::DefaultDom>, Vec<csp::IntVar>), csp::PropError> {
    assert!(n > 0);
    let mut model = csp::Model::new();
    let vars = model
        .int_var()
        .from_bounds(0, n - 1)
        .create_array(n as usize);

    model.all_diff(&vars).post()?;
    model
        .all_diff(
            vars.iter()
                .enumerate()
                .map(|(row, &var)| var.offset(row as i64)),
        ).post()?;
    model
        .all_diff(
            vars.iter()
                .enumerate()
                .map(|(row, &var)| var.offset(-(row as i64))),
        ).post()?;

    Ok((model, vars))
}

pub fn print_soln(soln: &csp::Solution<csp::DefaultDom>, n: i64, vars: &[csp::IntVar]) {
    let border_row = format!("+{:->width$}+", "", width = (2 * n + 1) as usize);
    let board = vars
        .iter()
        .map(|&var| {
            format!(
                "| {} |",
                (0..n)
                    .map(move |r| if r == soln.val(var) { 'Q' } else { '.' })
                    .intersperse(' ')
                    .collect::<String>()
            )
        }).intersperse("\n".to_owned())
        .collect::<String>();
    println!("{}\n{}\n{}", border_row, board, border_row);
}
