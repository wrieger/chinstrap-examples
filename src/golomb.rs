use chinstrap as csp;

pub fn subcommand() -> (&'static str, clap::App<'static, 'static>) {
    let name = "golomb";
    let subcommand = clap::SubCommand::with_name(name)
        .about("Finds an optimal Golomb ruler of the given order")
        .arg(
            clap::Arg::with_name("ORDER")
                .help("The number of marks on the Golomb ruler")
                .default_value("8"),
        );
    (name, subcommand)
}

pub fn handle_matches(matches: &clap::ArgMatches) {
    let order = matches
        .value_of("ORDER")
        .unwrap()
        .parse()
        .expect("could not parse ORDER as an integer");
    assert!(order > 0, "ORDER must be positive");

    let (model, marks) = model(order).expect("bad model");
    for soln in model
        .minimize(marks[(order - 1) as usize])
        .branch_on(&marks)
        .choose_var_by(csp::VarStrategy::InOrder)
        .choose_val_by(csp::ValStrategy::Minimum)
    {
        print_soln(&soln, &marks);
    }
}

pub fn model(order: i64) -> Result<(csp::Model<csp::DefaultDom>, Vec<csp::IntVar>), csp::PropError> {
    let mut model = csp::Model::new();

    let marks = model
        .int_var()
        .from_bounds(0, order * order)
        .create_array(order as usize);
    model.eq(marks[0], 0).post()?;
    for i in 0..((order - 1) as usize) {
        model.lt(marks[i], marks[i + 1]).post()?;
    }
    model
        .lt(
            marks[1] - marks[0],
            marks[(order - 1) as usize] - marks[(order - 2) as usize],
        ).post()?;
    let mut differences = Vec::with_capacity((order * (order - 1) / 2) as usize);
    for i in 0..(order as usize) {
        for j in (i + 1)..(order as usize) {
            let var = model.int_var().from_bounds(0, order * order).create();
            model.eq(marks[j] - marks[i], var).post()?;
            differences.push(var);
        }
    }
    model.all_diff(&differences).post()?;

    Ok((model, marks))
}

pub fn print_soln(soln: &csp::Solution<csp::DefaultDom>, marks: &[csp::IntVar]) {
    let vals = marks.iter().map(|&mark| soln.val(mark)).collect::<Vec<_>>();
    println!("{:?}", vals);
}
