use chinstrap as csp;
use itertools::Itertools;

pub fn subcommand() -> (&'static str, clap::App<'static, 'static>) {
    let subcommand = clap::SubCommand::with_name("queen_armies")
        .about("Solves the peaceable armies of queens problem")
        .arg(
            clap::Arg::with_name("model")
                .short("m")
                .long("model")
                .help("The model to use")
                .possible_values(&["naive", "advanced"])
                .default_value("advanced"),
        ).arg(
            clap::Arg::with_name("DIM")
                .help("The size of the board")
                .default_value("4"),
        );
    ("queen_armies", subcommand)
}

pub fn handle_matches(matches: &clap::ArgMatches) {
    let model_name = matches.value_of("model").unwrap();

    let dim = matches
        .value_of("DIM")
        .unwrap()
        .parse()
        .expect("could not parse DIM as an integer");
    assert!(dim > 0, "DIM must be positive");

    if model_name == "naive" {
        let (model, black_queens, white_queens, army_size) = model_naive(dim).expect("bad model");
        for soln in model
            .maximize(army_size)
            .branch_on(itertools::flatten(&black_queens))
            .choose_var_by(csp::VarStrategy::InOrder)
            .choose_val_by(csp::ValStrategy::Maximum)
        {
            print_soln_naive(&soln, dim, &black_queens, &white_queens);
        }
    } else if model_name == "advanced" {
        let (model, queens, army_size) = model_advanced(dim).expect("bad model");
        for soln in model
            .maximize(army_size)
            .branch_on(itertools::flatten(&queens))
            .choose_var_by(csp::VarStrategy::FailFirst)
            .choose_val_by(csp::ValStrategy::Maximum)
        {
            print_soln_advanced(&soln, dim, &queens);
        }
    } else {
        unreachable!()
    }
}

pub fn model_naive(
    dim: i64,
) -> Result<
    (
        csp::Model<csp::DefaultDom>,
        Vec<Vec<csp::IntVar>>,
        Vec<Vec<csp::IntVar>>,
        csp::IntVar,
    ),
    csp::PropError,
> {
    let mut model = csp::Model::new();

    let black_queens = model
        .int_var()
        .from_bounds(0, 1)
        .create_matrix(dim as usize, dim as usize);
    let white_queens = model
        .int_var()
        .from_bounds(0, 1)
        .create_matrix(dim as usize, dim as usize);
    let army_size = model.int_var().from_bounds(0, dim * dim).create();

    for (r_black, c_black, r_white, c_white) in iproduct!(0..dim, 0..dim, 0..dim, 0..dim) {
        if r_black == r_white
            || c_black == c_white
            || r_black + c_black == r_white + c_white
            || r_black + c_white == r_white + c_black
        {
            let black_queen = black_queens[r_black as usize][c_black as usize];
            let white_queen = white_queens[r_white as usize][c_white as usize];
            model.le(black_queen + white_queen, 1).post()?;
        }
    }

    model
        .count(itertools::flatten(&black_queens), 1, army_size)
        .post()?;
    model
        .count(itertools::flatten(&white_queens), 1, army_size)
        .post()?;

    Ok((model, black_queens, white_queens, army_size))
}

pub fn model_advanced(
    dim: i64,
) -> Result<
    (
        csp::Model<csp::DefaultDom>,
        Vec<Vec<csp::IntVar>>,
        csp::IntVar,
    ),
    csp::PropError,
> {
    let mut model = csp::Model::new();

    let queens = model
        .int_var()
        .from_bounds(0, 2)
        .create_matrix(dim as usize, dim as usize);
    let army_size = model.int_var().from_bounds(0, dim * dim).create();

    let rows = model.int_var().from_bounds(0, 1).create_array(dim as usize);
    let cols = model.int_var().from_bounds(0, 1).create_array(dim as usize);
    let diags0 = model
        .int_var()
        .from_bounds(0, 1)
        .create_array((2 * dim - 1) as usize);
    let diags1 = model
        .int_var()
        .from_bounds(0, 1)
        .create_array((2 * dim - 1) as usize);

    for r in 0..(dim as usize) {
        for c in 0..(dim as usize) {
            model.ne(queens[r][c] + rows[r], 2).post()?;
            model.ne(queens[r][c] + cols[c], 2).post()?;
            model.ne(queens[r][c] + diags0[r + c], 2).post()?;
            model
                .ne(queens[r][c] + diags1[(dim as usize - r - 1) + c], 2)
                .post()?;
        }
    }

    model
        .count(itertools::flatten(&queens), 1, army_size)
        .post()?;
    model
        .count(itertools::flatten(&queens), 2, army_size)
        .post()?;

    Ok((model, queens, army_size))
}

pub fn print_soln_naive(
    soln: &csp::Solution<csp::DefaultDom>,
    dim: i64,
    black_queens: &[Vec<csp::IntVar>],
    white_queens: &[Vec<csp::IntVar>],
) {
    let border_row = format!("+{:->width$}+", "", width = (2 * dim + 1) as usize);
    let board = (0..dim)
        .map(|r| {
            format!(
                "| {} |",
                (0..dim)
                    .map(
                        move |c| if soln.val(black_queens[r as usize][c as usize]) == 1 {
                            'B'
                        } else if soln.val(white_queens[r as usize][c as usize]) == 1 {
                            'W'
                        } else {
                            '.'
                        }
                    ).intersperse(' ')
                    .collect::<String>()
            )
        }).intersperse("\n".to_owned())
        .collect::<String>();
    println!("{}\n{}\n{}", border_row, board, border_row);
}

pub fn print_soln_advanced(soln: &csp::Solution<csp::DefaultDom>, dim: i64, queens: &[Vec<csp::IntVar>]) {
    let border_row = format!("+{:->width$}+", "", width = (2 * dim + 1) as usize);
    let board = (0..dim)
        .map(|r| {
            format!(
                "| {} |",
                (0..dim)
                    .map(move |c| if soln.val(queens[r as usize][c as usize]) == 1 {
                        'B'
                    } else if soln.val(queens[r as usize][c as usize]) == 2 {
                        'W'
                    } else {
                        '.'
                    }).intersperse(' ')
                    .collect::<String>()
            )
        }).intersperse("\n".to_owned())
        .collect::<String>();
    println!("{}\n{}\n{}", border_row, board, border_row);
}
