use chinstrap as csp;
use itertools::Itertools;

use crate::Example;

pub struct MagicHexagon;

impl Example for MagicHexagon {
    fn name(&self) -> &'static str {
        "magic_hexagon"
    }

    fn subcommand(&self) -> clap::App<'static, 'static> {
        clap::SubCommand::with_name(self.name())
            .about("Generates a normal magic hexagon of order 3")
    }

    fn handle_matches(&self, _: &clap::ArgMatches) -> Result<(), csp::PropError> {
        let radius = 2;
        let n_cells = 19;
        let magic_sum = 38;
        let coords = HexCoord::all_coords_within_radius(radius);
        assert_eq!(n_cells as usize, coords.len());

        let mut model = csp::Model::new();
        let cells = model
            .int_var()
            .from_bounds(1, n_cells)
            .create_array(n_cells as usize);

        model.all_diff(&cells).post()?;

        let slice_x = |x| {
            coords
                .iter()
                .zip(cells.iter())
                .filter(move |&(&coord, _)| coord.x == x)
                .map(|(_, &cell)| cell)
        };
        let slice_y = |y| {
            coords
                .iter()
                .zip(cells.iter())
                .filter(move |&(&coord, _)| coord.y == y)
                .map(|(_, &cell)| cell)
        };
        let slice_z = |z| {
            coords
                .iter()
                .zip(cells.iter())
                .filter(move |&(&coord, _)| coord.z == z)
                .map(|(_, &cell)| cell)
        };

        for i in -radius..=radius {
            model
                .sum_eq(slice_x(i), csp::var::const_var(magic_sum))
                .post()?;
            model
                .sum_eq(slice_y(i), csp::var::const_var(magic_sum))
                .post()?;
            model
                .sum_eq(slice_z(i), csp::var::const_var(magic_sum))
                .post()?;
        }

        // symmetry breaking
        let corner_cells: Vec<_> = coords
            .iter()
            .zip(cells.iter())
            .filter(|&(&coord, _)| HexCoord::corners(radius).contains(&coord))
            .map(|(_, &cell)| cell)
            .collect();
        model.lt(corner_cells[0], corner_cells[1]).post()?;
        model.lt(corner_cells[0], corner_cells[2]).post()?;
        model.lt(corner_cells[0], corner_cells[3]).post()?;
        model.lt(corner_cells[0], corner_cells[4]).post()?;
        model.lt(corner_cells[0], corner_cells[5]).post()?;
        model.lt(corner_cells[1], corner_cells[2]).post()?;

        &model
            .solve()
            .choose_var_by(csp::VarStrategy::FailFirst)
            .choose_val_by(csp::ValStrategy::Minimum)
            .on_solution(move |soln| print_soln(soln, &coords, &cells))
            .collect::<Vec<_>>();

        Ok(())
    }
}

#[derive(Clone, Copy, Debug, Eq, Ord, PartialEq, PartialOrd)]
pub struct HexCoord {
    pub x: i64,
    pub y: i64,
    pub z: i64,
}

impl HexCoord {
    pub fn all_coords_within_radius(radius: i64) -> Vec<HexCoord> {
        assert!(radius >= 0);
        let mut coords = Vec::with_capacity((3 * radius * radius - 3 * radius + 1) as usize);
        for x in -radius..=radius {
            for y in -radius..=radius {
                let coord = HexCoord { x, y, z: -x - y };
                if coord.radius() <= radius {
                    coords.push(coord);
                }
            }
        }
        coords
    }

    pub fn corners(radius: i64) -> Vec<HexCoord> {
        assert!(radius > 0);
        vec![
            HexCoord {
                x: -radius,
                y: radius,
                z: 0,
            },
            HexCoord {
                x: -radius,
                y: 0,
                z: radius,
            },
            HexCoord {
                x: 0,
                y: -radius,
                z: radius,
            },
            HexCoord {
                x: radius,
                y: -radius,
                z: 0,
            },
            HexCoord {
                x: radius,
                y: 0,
                z: -radius,
            },
            HexCoord {
                x: 0,
                y: radius,
                z: -radius,
            },
        ]
    }

    fn radius(&self) -> i64 {
        self.x.abs().max(self.y.abs()).max(self.z.abs())
    }
}

pub fn print_soln(
    soln: &csp::Solution<csp::DefaultDom>,
    coords: &[HexCoord],
    cells: &[csp::IntVar],
) {
    let radius = 2;
    let width = 2;
    let slice_x = |x| {
        coords
            .iter()
            .zip(cells.iter())
            .filter(move |&(&coord, _)| coord.x == x)
            .map(|(_, &cell)| cell)
    };
    let hexagon = (-radius..=radius)
        .map(|r| {
            let vals = slice_x(r)
                .map(|cell| format!("{:width$}", soln.val(cell), width = width))
                .intersperse(format!("{:width$}", "", width = width))
                .collect::<String>();
            let padding = format!("{:width$}", "", width = width * (r.abs() as usize));
            format!("{}{}", padding, vals)
        }).intersperse("\n\n".to_owned())
        .collect::<String>();
    println!("{}", hexagon);
}
